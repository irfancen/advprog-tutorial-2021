package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.flavor.Umami;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.meat.Fish;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.noodle.Shirataki;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.topping.Flower;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.topping.Topping;

public class SnevnezhaShiratakiIngredientFactory implements MenuIngredientFactory {

    @Override
    public Flavor createFlavor() {
        return new Umami();
    }

    @Override
    public Meat createMeat() {
        return new Fish();
    }

    @Override
    public Noodle createNoodle() {
        return new Shirataki();
    }

    @Override
    public Topping createTopping() {
        return new Flower();
    }
}
