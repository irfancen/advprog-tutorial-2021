package csui.advpro2021.tais.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.LogSummary;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.service.LogServiceImpl;
import csui.advpro2021.tais.service.MahasiswaServiceImpl;
import csui.advpro2021.tais.service.MataKuliahServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


import java.util.*;

@WebMvcTest(controllers = LogController.class)
public class LogControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private LogServiceImpl logService;

    @MockBean
    private MahasiswaServiceImpl mahasiswaService;

    @MockBean
    private MataKuliahServiceImpl mataKuliahService;

    private Log log;
    private Mahasiswa mahasiswa;
    private MataKuliah mataKuliah;

    @BeforeEach
    public void setUp() {
        mahasiswa = new Mahasiswa("1906293202", "Muhammad Irfan Junaidi", "muhammad.irfan96@ui.ac.id", "4", "081111111111");
        Calendar calendar = Calendar.getInstance();
        calendar.set(2000, 1, 1, 12, 0, 0);
        Date start = calendar.getTime();
        calendar.set(2000, 1, 1, 16, 0, 0);
        Date end = calendar.getTime();

        log = new Log();
        log.setIdLog(1);
        log.setStart(start);
        log.setEnd(end);
        log.setDesc("Description");
        log.setMahasiswa(mahasiswa);

        mataKuliah = new MataKuliah("001", "Satu", "Ilmu Komputer");
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    public void testControllerGetListLog() throws Exception{

        Iterable<Log> listLog = Arrays.asList(log);
        when(logService.getListLog()).thenReturn(listLog);

        mvc.perform(get("/asisten/logs/").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].idLog").value("1"));
    }

    @Test
    public void testLogControllerGetLogById() throws Exception{
        when(logService.getLogByIdLog(log.getIdLog())).thenReturn(log);

        mvc.perform(get("/asisten/log/1").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.desc").value("Description"));
    }

    @Test
    public void testLogControllerGetLogByIdNotFound() throws Exception {
        when(logService.getLogByIdLog(log.getIdLog())).thenReturn(log);

        mvc.perform(get("/asisten/log/1567")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testLogControllerAddAsisten() throws Exception {
        when(mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm())).thenReturn(mahasiswa);
        when(mataKuliahService.getMataKuliah(mataKuliah.getKodeMatkul())).thenReturn(mataKuliah);
        when(mahasiswaService.addAsisten(mahasiswa, mataKuliah)).thenReturn(mahasiswa);

        mvc.perform(post("/asisten/daftar/" + mahasiswa.getNpm() + "/" + mataKuliah.getKodeMatkul())
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.npm").value(mahasiswa.getNpm()));
    }

    @Test
    public void testLogControllerPostAddMahasiswaNotFound() throws Exception {
        when(mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm())).thenReturn(null);

        mvc.perform(post("/asisten/daftar/" + mahasiswa.getNpm() + "/" + mataKuliah.getKodeMatkul())
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testLogControllerPostAddMataKuliahNotFound() throws Exception {
        when(mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm())).thenReturn(mahasiswa);

        mvc.perform(post("/asisten/daftar/" + mahasiswa.getNpm() + "/" + mataKuliah.getKodeMatkul())
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testLogControllerDeleteLog() throws Exception {
        when(logService.getLogByIdLog(log.getIdLog())).thenReturn(log);

        mvc.perform(delete("/asisten/log/" + log.getIdLog()))
                .andExpect(status().isNoContent());
    }

    @Test
    public void testLogControllerDeleteLogNotFound() throws Exception {
        mvc.perform(delete("/asisten/log/" + log.getIdLog()))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testLogControllerGetSummary() throws Exception {
        mahasiswa.setMataKuliah(mataKuliah);
        when(mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm())).thenReturn(mahasiswa);
        List<LogSummary> listLogSummary = new ArrayList<>();
        listLogSummary.add(new LogSummary("January", 4, 1400));
        when(logService.getLogSummary(mahasiswa)).thenReturn(listLogSummary);

        mvc.perform(get("/asisten/summary/" + mahasiswa.getNpm()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].month").value("January"));
    }

    @Test
    public void testLogControllerGetSummaryNotFound() throws Exception {
        mvc.perform(get("/asisten/summary/" + mahasiswa.getNpm()))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testLogControllerCreateLog() throws Exception {
        mahasiswa.setMataKuliah(mataKuliah);
        when(mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm())).thenReturn(mahasiswa);
        when(logService.createLog(any(Mahasiswa.class), any(Log.class))).thenReturn(log);

        String logStr = "{\"start\":946702800000,\"end\":946717200000,\"desc\":\"Description\"}";

        mvc.perform(post("/asisten/create/" + mahasiswa.getNpm())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(logStr))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.desc").value("Description"));
    }

    @Test
    public void testLogControllerCreateLogMahasiswaNotFound() throws Exception {
        String logStr = "{\"start\":946702800000,\"end\":946717200000,\"desc\":\"Description\"}";

        mvc.perform(post("/asisten/create/" + mahasiswa.getNpm())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(logStr))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testLogControllerGetLogsByMahasiswa() throws Exception {
        mahasiswa.setMataKuliah(mataKuliah);
        when(mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm())).thenReturn(mahasiswa);
        List<Log> listLog = new ArrayList<>();
        listLog.add(log);
        when(logService.getAllLogByMahasiswa(mahasiswa)).thenReturn(listLog);

        mvc.perform(get("/asisten/logs/" + mahasiswa.getNpm()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].desc").value("Description"));
    }

    @Test
    public void testLogControllerGetLogsByMahasiswaMahasiswaNotFound() throws Exception {
        mvc.perform(get("/asisten/logs/" + mahasiswa.getNpm()))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testLogControllerUpdateLog() throws Exception {
        when(logService.getLogByIdLog(log.getIdLog())).thenReturn(log);
        when(mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm())).thenReturn(mahasiswa);

        Calendar calendar = Calendar.getInstance();
        calendar.set(2000, 1, 1, 12, 30, 0);
        Date start = calendar.getTime();
        calendar.set(2000, 1, 1, 17, 30, 0);
        Date end = calendar.getTime();

        Log log2 = new Log(start, end, "DescriptionEdited");

        String logStr = "{\"start\":946704600000,\"end\":946722600000,\"desc\":\"DescriptionEdited\"}";

        when(logService.updateLog(anyInt(), any(Mahasiswa.class), any(Log.class))).thenReturn(log2);
        mvc.perform(put("/asisten/log/" + log.getIdLog())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(logStr))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.desc").value("DescriptionEdited"));
    }

    @Test
    public void testLogControllerUpdateLogMahasiswaNotFound() throws Exception {
        String logStr = "{\"start\":946704600000,\"end\":946722600000,\"desc\":\"DescriptionEdited\"}";

        mvc.perform(put("/asisten/log/" + log.getIdLog())
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(logStr))
                .andExpect(status().isNotFound());
    }
}
