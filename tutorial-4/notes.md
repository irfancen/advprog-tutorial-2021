## Perbedaan Lazy dan Eager Instantiation
1. Lazy Instantiation membuat objek hanya pada saat dibutuhkan, sedangkan Eager Instantiation membuat objek sejak awal aplikasi berjalan.
2. Lazy Instantiation memiliki isu multithreading, sedangkan Eager Instantiation tidak.
3. Lazy Instantiation dapat dilakukan Exception Handling, sedangkan Eager Instantiation tidak.
## Lazy Instantiation
### Keuntungan
1. Karena objek dibuat hanya pada saat dibutuhkan, tidak memakan resource sebelum objek dibuat.
2. Dapat dilakukan Exception handling
### Kekurangan
1. Tidak thread-safe, dapat merusak properti singleton
2. Kondisi null harus selalu dicek
## Eager Instantiation
### Keuntungan
1. Thread-safe karena merupakan Static initialization
2. Mudah untuk diimplementasi
### Kekurangan
1. Karena objek dibuat sejak awal, akan memakan resource lebih meskipun belum tentu objek tersebut dipakai.
2. Waktu terbuang untuk membuat instance yang belum tentu dipakai
3. Tidak dapat dilakukan Exception handling