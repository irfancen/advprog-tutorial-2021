package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.factory.MondoUdonIngredientFactory;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.flavor.Salty;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.meat.Chicken;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.noodle.Udon;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.topping.Cheese;

public class MondoUdon extends Menu {
    // Ingredients:
    // Noodle: Udon
    // Meat: Chicken
    // Topping: Cheese
    // Flavor: Salty
    public MondoUdon(String name){
        super(name, new MondoUdonIngredientFactory());
    }
}