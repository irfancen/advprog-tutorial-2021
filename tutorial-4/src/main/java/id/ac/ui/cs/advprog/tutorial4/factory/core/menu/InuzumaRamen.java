package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.factory.InuzumaRamenIngredientFactory;
import id.ac.ui.cs.advprog.tutorial4.factory.core.factory.MenuIngredientFactory;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.flavor.Spicy;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.meat.Pork;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.noodle.Ramen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.topping.BoiledEgg;

public class InuzumaRamen extends Menu {
    // Ingredients:
    // Noodle: Ramen
    // Meat: Pork
    // Topping: Boiled Egg
    // Flavor: Spicy
    public InuzumaRamen(String name) {
        super(name, new InuzumaRamenIngredientFactory());
    }
}