package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.factory.LiyuanSobaIngredientFactory;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.flavor.Sweet;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.meat.Beef;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.noodle.Soba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.topping.Mushroom;

public class LiyuanSoba extends Menu {
    // Ingredients:
    // Noodle: Soba
    // Meat: Beef
    // Topping: Mushroom
    // Flavor: Sweet
    public LiyuanSoba(String name){
        super(name, new LiyuanSobaIngredientFactory());
    }
}