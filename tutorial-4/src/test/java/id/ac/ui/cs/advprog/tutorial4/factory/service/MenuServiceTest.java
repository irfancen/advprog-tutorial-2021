package id.ac.ui.cs.advprog.tutorial4.factory.service;

import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.InuzumaRamen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.LiyuanSoba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.MondoUdon;
import id.ac.ui.cs.advprog.tutorial4.factory.repository.MenuRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class MenuServiceTest {
    private Class<?> menuServiceClass;

    @Spy
    MenuRepository menuRepository = new MenuRepository();

    @InjectMocks
    MenuService menuService = new MenuServiceImpl();

    @BeforeEach
    public void setup() throws Exception {
        menuServiceClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial4.factory.service.MenuServiceImpl");
    }

    @Test
    public void testMenuServiceHasGetMenusMethod() throws Exception {
        Method getMenus = menuServiceClass.getDeclaredMethod("getMenus");
        int methodModifiers = getMenus.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));

        ParameterizedType pt = (ParameterizedType) getMenus.getGenericReturnType();
        assertEquals(List.class, pt.getRawType());
        assertTrue(Arrays.asList(pt.getActualTypeArguments()).contains(Menu.class));
    }

    @Test
    public void testMenuServiceGetMenusReturnMenus() {
        List<Menu> mockListMenu = new ArrayList<>();
        mockListMenu.add(new InuzumaRamen("Inuzuma Ramen"));
        mockListMenu.add(new LiyuanSoba("Liyuan Soba"));

        when(menuRepository.getMenus()).thenReturn(mockListMenu);

        assertEquals(2, menuService.getMenus().size());
        verify(menuRepository, atLeastOnce()).getMenus();
    }

    @Test
    public void testMenuServiceHasCreateMenuMethod() throws Exception {
        Method createMenu = menuServiceClass.getDeclaredMethod("createMenu", String.class, String.class);
        int methodModifiers = createMenu.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(createMenu.getReturnType(), Menu.class);
    }

    @Test
    public void testMenuServiceCreateMenuAddMenu() {
        menuService.createMenu("Mondo Udon", "MondoUdon");
        assertEquals(1, menuRepository.getMenus().size());
        verify(menuRepository, atLeastOnce()).add(any(MondoUdon.class));
    }
}
