package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.factory.MenuIngredientFactory;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.topping.Topping;

public abstract class Menu {
    String name;
    Noodle noodle;
    Meat meat;
    Topping topping;
    Flavor flavor;
    MenuIngredientFactory menuIngredientFactory;

    public Menu(String name, MenuIngredientFactory menuIngredientFactory) {
        this.name = name;
        this.menuIngredientFactory = menuIngredientFactory;
        prepare();
    }

    void prepare() {
        this.noodle = menuIngredientFactory.createNoodle();
        this.meat = menuIngredientFactory.createMeat();
        this.topping = menuIngredientFactory.createTopping();
        this.flavor = menuIngredientFactory.createFlavor();
    }

    public String getName() {
        return name;
    }

    public Noodle getNoodle() {
        return noodle;
    }

    public Meat getMeat() {
        return meat;
    }

    public Topping getTopping() {
        return topping;
    }

    public Flavor getFlavor() {
        return flavor;
    }
}