package id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.noodle;

public class Udon implements Noodle {
    public String getDescription(){
        return "Adding Mondo Udon Noodles...";
    }
}