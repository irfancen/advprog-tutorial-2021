package id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.meat;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class FishTest {
    @Test
    public void testFishDescription() {
        Fish fish = new Fish();
        assertEquals("Adding Zhangyun Salmon Fish Meat...", fish.getDescription());
    }
}
