package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.LogSummary;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.repository.LogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class LogServiceImpl implements LogService {
    @Autowired
    private LogRepository logRepository;

    @Override
    public Log createLog(Mahasiswa mahasiswa, Log log) {
        log.setMahasiswa(mahasiswa);
        logRepository.save(log);
        return log;
    }

    @Override
    public Iterable<Log> getListLog() {
        return logRepository.findAll();
    }

    @Override
    public Log getLogByIdLog(Integer idLog) {
        return logRepository.findByIdLog(idLog);
    }

    @Override
    public List<Log> getAllLogByMahasiswa(Mahasiswa mahasiswa) {
        return logRepository.findAllLogByMahasiswa(mahasiswa);
    }

    @Override
    public Collection<LogSummary> getLogSummary(Mahasiswa mahasiswa) {
        Calendar calendar = Calendar.getInstance();
        HashMap<Integer, LogSummary> result = new HashMap<>();
        for (Log log : getAllLogByMahasiswa(mahasiswa)) {
            Date start = log.getStart();
            Date end = log.getEnd();
            int duration = (int) (end.getTime() - start.getTime()) / 3600000;

            calendar.setTime(start);
            String month = calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault());

            LogSummary logSummary = result.getOrDefault(Calendar.MONTH, new LogSummary(month));
            logSummary.setHours(logSummary.getHours() + duration);

            result.put(Calendar.MONTH, logSummary);
        }
        for (LogSummary logSummary : result.values()) {
            logSummary.setPayment(logSummary.getHours() * 350L);
        }
        return result.values();
    }

    @Override
    public Log updateLog(Integer idLog, Mahasiswa mahasiswa, Log log) {
        log.setIdLog(idLog);
        log.setMahasiswa(mahasiswa);
        logRepository.save(log);
        return log;
    }

    @Override
    public void deleteLogByIdLog(Integer idLog) {
        logRepository.deleteById(idLog);
    }
}
