package id.ac.ui.cs.advprog.tutorial4.singleton.core;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class OrderFoodTest {
    // Insert Tests Here
    @Test
    public void testOrderDrinkToStringNotNullMethod(){
        OrderFood orderFood = OrderFood.getInstance();
        orderFood.setFood("Rojo Lele");
        assertEquals("Rojo Lele", orderFood.toString());
    }
}
