package id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.flavor;

public class Umami implements Flavor {
    public String getDescription(){
        return "Adding WanPlus Specialty MSG flavoring...";
    }
}