package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.flavor.Salty;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.meat.Chicken;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.noodle.Udon;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.topping.Cheese;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.topping.Topping;

public class MondoUdonIngredientFactory implements MenuIngredientFactory {

    @Override
    public Flavor createFlavor() {
        return new Salty();
    }

    @Override
    public Meat createMeat() {
        return new Chicken();
    }

    @Override
    public Noodle createNoodle() {
        return new Udon();
    }

    @Override
    public Topping createTopping() {
        return new Cheese();
    }
}
