# Requirements

## Entities
1. Mahasiswa
2. Mata Kuliah
3. Log Asisten

### Mahasiswa
- Mahasiswa mendaftar mata kuliah
- Mahasiswa dapat membuat/menghapus log

### Mata Kuliah
- Mahasiswa yang terdaftar pada mata kuliah tidak dapat dihapus
- Mata Kuliah yang sudah didaftar Mahasiswa tidak dapat dihapus

### Log Asisten
- Log berisi reference ke Mahasiswa yang bersangkutan
- Terdapat summary dari Log yang merupakan akumulasi jam dan bayaran per bulan