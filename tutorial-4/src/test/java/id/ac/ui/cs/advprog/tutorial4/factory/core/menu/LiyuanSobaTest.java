package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.flavor.Sweet;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.meat.Beef;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.noodle.Soba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.topping.Mushroom;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class LiyuanSobaTest {
    Menu liyuanSoba = new LiyuanSoba("Liyuan Soba");

    @Test
    public void testLiyuanSobaGetName() {
        assertEquals("Liyuan Soba", liyuanSoba.getName());
    }

    @Test
    public void testLiyuanSobaGetNoodle() {
        assertTrue(liyuanSoba.getNoodle() instanceof Soba);
    }

    @Test
    public void testLiyuanSobaGetMeat() {
        assertTrue(liyuanSoba.getMeat() instanceof Beef);
    }

    @Test
    public void testLiyuanSobaGetTopping() {
        assertTrue(liyuanSoba.getTopping() instanceof Mushroom);
    }

    @Test
    public void testLiyuanSobaGetFlavor() {
        assertTrue(liyuanSoba.getFlavor() instanceof Sweet);
    }
}
