package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.flavor.Sweet;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.meat.Beef;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.noodle.Soba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.topping.Mushroom;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.topping.Topping;

public class LiyuanSobaIngredientFactory implements MenuIngredientFactory {

    @Override
    public Flavor createFlavor() {
        return new Sweet();
    }

    @Override
    public Meat createMeat() {
        return new Beef();
    }

    @Override
    public Noodle createNoodle() {
        return new Soba();
    }

    @Override
    public Topping createTopping() {
        return new Mushroom();
    }
}
