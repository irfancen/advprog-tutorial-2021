package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class AgileAdventurer extends Adventurer {


    public AgileAdventurer(Guild guild) {
        this.name = "Agile";
        this.guild = guild;
    }

    @Override
    public void update() {
        switch (guild.getQuestType()) {
            case "D":
            case "R":
                getQuests().add(guild.getQuest());
                break;
            case "E":
                break;
        }
    }
}
