package id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.topping;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class FlowerTest {
    @Test
    public void testFlowerDescription() {
        Flower flower = new Flower();
        assertEquals("Adding Xinqin Flower Topping...", flower.getDescription());
    }
}
