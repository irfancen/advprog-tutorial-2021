package id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.topping;

public class Cheese implements Topping {
    public String getDescription(){
        return "Adding Shredded Cheese Topping...";
    }
}