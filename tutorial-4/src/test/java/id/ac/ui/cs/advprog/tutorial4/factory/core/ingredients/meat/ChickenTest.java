package id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.meat;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class ChickenTest {
    @Test
    public void testChickenDescription() {
        Chicken chicken = new Chicken();
        assertEquals("Adding Wintervale Chicken Meat...", chicken.getDescription());
    }
}
