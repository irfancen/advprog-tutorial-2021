package csui.advpro2021.tais.model;

import com.fasterxml.jackson.annotation.JsonGetter;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "log")
@Data
@NoArgsConstructor
public class Log {
    @Id
    @Column(name = "id_log", updatable = false, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idLog;

    @Column(name = "start_time")
    private Date start;

    @Column(name = "end_time")
    private Date end;

    @Column(name = "description")
    private String desc;

    @ManyToOne
    @JoinColumn(name = "mahasiswa", nullable = false)
    private Mahasiswa mahasiswa;

    public Log(Date start, Date end, String desc) {
        this.start = start;
        this.end = end;
        this.desc = desc;
    }

    @JsonGetter("mahasiswa")
    public String getJsonMataKuliah() {
        return mahasiswa == null ? "" : mahasiswa.getNpm();
    }
}
