package id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.topping;

public class BoiledEgg implements Topping {
    public String getDescription(){
        return "Adding Guahuan Boiled Egg Topping";
    }
}