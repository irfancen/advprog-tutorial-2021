package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.flavor.Salty;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.meat.Chicken;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.noodle.Udon;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.topping.Cheese;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MondoUdonTest {
    Menu mondoUdon = new MondoUdon("Mondo Udon");

    @Test
    public void testMondoUdonGetName() {
        assertEquals("Mondo Udon", mondoUdon.getName());
    }

    @Test
    public void testMondoUdonGetNoodle() {
        assertTrue(mondoUdon.getNoodle() instanceof Udon);
    }

    @Test
    public void testMondoUdonGetMeat() {
        assertTrue(mondoUdon.getMeat() instanceof Chicken);
    }

    @Test
    public void testMondoUdonGetTopping() {
        assertTrue(mondoUdon.getTopping() instanceof Cheese);
    }

    @Test
    public void testMondoUdonGetFlavor() {
        assertTrue(mondoUdon.getFlavor() instanceof Salty);
    }
}
