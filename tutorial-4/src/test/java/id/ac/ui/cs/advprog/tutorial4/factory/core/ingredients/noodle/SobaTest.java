package id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.noodle;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class SobaTest {
    @Test
    public void testSobaDescription() {
        Soba soba = new Soba();
        assertEquals("Adding Liyuan Soba Noodles...", soba.getDescription());
    }
}
