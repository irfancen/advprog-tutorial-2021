package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class MysticAdventurer extends Adventurer {


    public MysticAdventurer(Guild guild) {
        this.name = "Mystic";
        this.guild = guild;
    }

    @Override
    public void update() {
        switch (guild.getQuestType()) {
            case "D":
            case "E":
                getQuests().add(guild.getQuest());
                break;
            case "R":
                break;
        }
    }
}
