package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.LogSummary;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.repository.LogRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
public class LogServiceImplTest {
    @Mock
    private LogRepository logRepository;

    @InjectMocks
    private LogServiceImpl logService;

    private Log log;
    private Mahasiswa mahasiswa;

    @BeforeEach
    public void setUp() {
        mahasiswa = new Mahasiswa("1906293202", "Muhammad Irfan Junaidi", "muhammad.irfan96@ui.ac.id", "4", "081111111111");

        Calendar calendar = Calendar.getInstance();
        calendar.set(2000, 1, 1, 12, 0, 0);
        Date start = calendar.getTime();
        calendar.set(2000, 1, 1, 16, 0, 0);
        Date end = calendar.getTime();

        log = new Log();
        log.setIdLog(1);
        log.setStart(start);
        log.setEnd(end);
        log.setDesc("Description");
        log.setMahasiswa(mahasiswa);
    }

    @Test
    public void testServiceCreateLog() {
        lenient().when(logService.createLog(mahasiswa, log)).thenReturn(log);
    }

    @Test
    public void testLogServiceGetListLog() {
        Iterable<Log> listLog = logRepository.findAll();
        lenient().when(logService.getListLog()).thenReturn(listLog);
        Iterable<Log> listLogs = logService.getListLog();
        assertIterableEquals(listLog, listLogs);
    }

    @Test
    public void testServiceGetLogByIdLog() {
        lenient().when(logService.getLogByIdLog(1)).thenReturn(log);
        Log resLog = logService.getLogByIdLog(log.getIdLog());
        assertEquals(log.getIdLog(), resLog.getIdLog());
    }

    @Test
    public void testLogDeleteLog() {
        logService.createLog(mahasiswa, log);
        logService.deleteLogByIdLog(log.getIdLog());
        lenient().when(logService.getLogByIdLog(log.getIdLog())).thenReturn(null);
        assertNull(logService.getLogByIdLog(log.getIdLog()));
    }

    @Test
    public void testServiceUpdateLog() {
        logService.createLog(mahasiswa, log);
        String desc = log.getDesc();
        log.setDesc("DescriptionEdited");

        lenient().when(logService.updateLog(log.getIdLog(), mahasiswa, log)).thenReturn(log);
        Log resLog = logService.updateLog(log.getIdLog(), mahasiswa, log);

        assertNotEquals(resLog.getDesc(), desc);
        assertEquals(resLog.getEnd(), log.getEnd());
    }

    @Test
    public void testServiceGetSummary() {
        List<Log> logList = new ArrayList<>();
        logList.add(log);
        lenient().when(logRepository.findAllLogByMahasiswa(mahasiswa)).thenReturn(logList);

        List<LogSummary> expectedLogList = new ArrayList<>();
        expectedLogList.add(new LogSummary("February", 4, 1400));

        Collection<LogSummary> listLogSummary = logService.getLogSummary(mahasiswa);
        assertIterableEquals(expectedLogList, listLogSummary);
    }

}
