package id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.topping;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class BoiledEggTest {
    @Test
    public void testBoiledEggDescription() {
        BoiledEgg boiledEgg = new BoiledEgg();
        assertEquals("Adding Guahuan Boiled Egg Topping", boiledEgg.getDescription());
    }
}
