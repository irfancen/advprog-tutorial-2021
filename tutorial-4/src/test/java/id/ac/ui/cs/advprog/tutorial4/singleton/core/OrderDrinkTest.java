package id.ac.ui.cs.advprog.tutorial4.singleton.core;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class OrderDrinkTest {
    // Insert Tests Here
    @Test
    public void testOrderDrinkToStringNotNullMethod(){
        OrderDrink orderDrink = OrderDrink.getInstance();
        orderDrink.setDrink("Aqua");
        assertEquals("Aqua", orderDrink.toString());
    }
}
