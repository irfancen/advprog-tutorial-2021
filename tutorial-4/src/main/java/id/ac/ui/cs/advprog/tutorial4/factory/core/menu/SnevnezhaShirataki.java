package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.factory.SnevnezhaShiratakiIngredientFactory;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.flavor.Umami;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.meat.Fish;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.noodle.Shirataki;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.topping.Flower;

public class SnevnezhaShirataki extends Menu {
    // Ingredients:
    // Noodle: Shirataki
    // Meat: Fish
    // Topping: Flower
    // Flavor: Umami
    public SnevnezhaShirataki(String name){
        super(name, new SnevnezhaShiratakiIngredientFactory());
    }
}