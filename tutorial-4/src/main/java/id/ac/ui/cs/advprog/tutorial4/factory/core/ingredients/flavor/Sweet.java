package id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.flavor;

public class Sweet implements Flavor {
    public String getDescription(){
        return "Adding a dash of Sweet Soy Sauce...";
    }
}