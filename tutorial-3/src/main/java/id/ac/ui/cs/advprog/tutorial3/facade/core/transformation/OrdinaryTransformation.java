package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;

/**
 * Kelas ini melakukan cipher ROT13
 */
public class OrdinaryTransformation {
    private int key;

    public OrdinaryTransformation(){
        this.key = 13;
    }

    public Spell encode(Spell spell){
        return process(spell, true);
    }

    public Spell decode(Spell spell){
        return process(spell, false);
    }

    private Spell process(Spell spell, boolean encode) {
        String text = spell.getText();
        Codex codex = spell.getCodex();
        int selector = encode ? 1 : -1;
        int n = codex.getCharacters().length;
        char[] res = new char[text.length()];
        for(int i = 0; i < text.length(); i++){
            int c = codex.getIndex(text.charAt(i));
            int newIdx = (c + key * selector) % n;
            newIdx = newIdx < 0 ? n + newIdx : newIdx;
            res[i] = codex.getChar(newIdx);
        }

        return new Spell(new String(res), codex);
    }
}
