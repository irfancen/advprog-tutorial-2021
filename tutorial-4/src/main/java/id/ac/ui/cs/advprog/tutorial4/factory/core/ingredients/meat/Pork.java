package id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.meat;

public class Pork implements Meat {
    public String getDescription(){
        return "Adding Tian Xu Pork Meat...";
    }
}