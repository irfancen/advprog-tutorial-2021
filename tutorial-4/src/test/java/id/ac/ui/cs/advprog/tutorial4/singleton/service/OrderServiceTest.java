package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderDrink;
import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderFood;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
public class OrderServiceTest {
    private Class<?> orderServiceClass;
    private OrderServiceImpl orderService = new OrderServiceImpl();

    @BeforeEach
    public void setup() throws Exception {
        orderServiceClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial4.singleton.service.OrderServiceImpl");
    }

    @Test
    public void testOrderServiceHasOrderADrinkMethod() throws Exception {
        Method orderADrink = orderServiceClass.getDeclaredMethod("orderADrink", String.class);
        int methodModifiers = orderADrink.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(orderADrink.getReturnType(), void.class);
    }

    @Test
    public void testOrderServiceOrderADrinkMethod() {
        orderService.orderADrink("Water");
        assertEquals("Water", OrderDrink.getInstance().getDrink());
    }

    @Test
    public void testOrderServiceOrderGetDrinkMethod() {
        assertEquals(OrderDrink.getInstance(), orderService.getDrink());
    }

    @Test
    public void testOrderServiceHasOrderAFoodMethod() throws Exception {
        Method orderAFood = orderServiceClass.getDeclaredMethod("orderAFood", String.class);
        int methodModifiers = orderAFood.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(orderAFood.getReturnType(), void.class);
    }

    @Test
    public void testOrderServiceOrderAFoodMethod() {
        orderService.orderAFood("Rice");
        assertEquals("Rice", OrderFood.getInstance().getFood());
    }

    @Test
    public void testOrderServiceOrderGetFoodMethod() {
        assertEquals(OrderFood.getInstance(), orderService.getFood());
    }
}
