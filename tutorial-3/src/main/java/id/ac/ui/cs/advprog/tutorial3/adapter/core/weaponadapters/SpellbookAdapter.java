package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

public class SpellbookAdapter implements Weapon {

    private Spellbook spellbook;
    private boolean noMana;

    public SpellbookAdapter(Spellbook spellbook) { this.spellbook = spellbook; }

    @Override
    public String normalAttack() {
        noMana = false;
        return spellbook.smallSpell();
    }

    @Override
    public String chargedAttack() {
        if (!noMana) {
            noMana = true;
            return spellbook.largeSpell();
        }
        return "Not enough mana for large spell";

    }

    @Override
    public String getName() {
        return spellbook.getName();
    }

    @Override
    public String getHolderName() {
        return spellbook.getHolderName();
    }

}
