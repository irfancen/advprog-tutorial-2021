package id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.meat;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class BeefTest {
    @Test
    public void testBeefDescription() {
        Beef beef = new Beef();
        assertEquals("Adding Maro Beef Meat...", beef.getDescription());
    }
}
