package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.flavor.Umami;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.meat.Fish;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.noodle.Shirataki;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.topping.Flower;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class SnevnezhaShiratakiTest {
    Menu snevnezhaShirataki = new SnevnezhaShirataki("Snevnezha Shirataki");

    @Test
    public void testSnevnezhaShiratakiGetName() {
        assertEquals("Snevnezha Shirataki", snevnezhaShirataki.getName());
    }

    @Test
    public void testSnevnezhaShiratakiGetNoodle() {
        assertTrue(snevnezhaShirataki.getNoodle() instanceof Shirataki);
    }

    @Test
    public void testSnevnezhaShiratakiGetMeat() {
        assertTrue(snevnezhaShirataki.getMeat() instanceof Fish);
    }

    @Test
    public void testSnevnezhaShiratakiGetTopping() {
        assertTrue(snevnezhaShirataki.getTopping() instanceof Flower);
    }

    @Test
    public void testSnevnezhaShiratakiGetFlavor() {
        assertTrue(snevnezhaShirataki.getFlavor() instanceof Umami);
    }
}
