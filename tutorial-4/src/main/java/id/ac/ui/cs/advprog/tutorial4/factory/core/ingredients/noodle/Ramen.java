package id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.noodle;

public class Ramen implements Noodle {
    public String getDescription(){
        return "Adding Inuzuma Ramen Noodles...";
    }
}