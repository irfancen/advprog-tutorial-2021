package csui.advpro2021.tais.model;

import lombok.Data;

@Data
public class LogSummary {
    String month;
    int hours;
    long payment;

    public LogSummary(String month, int hours, long payment){
        this.month = month;
        this.hours = hours;
        this.payment = payment;
    }

    public LogSummary(String month) {
        this(month, 0, 0);
    }
}
