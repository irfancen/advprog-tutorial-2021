package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.flavor.Spicy;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.meat.Pork;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.noodle.Ramen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.topping.BoiledEgg;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class InuzumaRamenTest {
    Menu inuzumaRamen = new InuzumaRamen("Inuzuma Ramen");

    @Test
    public void testInuzumaRamenGetName() {
        assertEquals("Inuzuma Ramen", inuzumaRamen.getName());
    }

    @Test
    public void testInuzumaRamenGetNoodle() {
        assertTrue(inuzumaRamen.getNoodle() instanceof Ramen);
    }

    @Test
    public void testInuzumaRamenGetMeat() {
        assertTrue(inuzumaRamen.getMeat() instanceof Pork);
    }

    @Test
    public void testInuzumaRamenGetTopping() {
        assertTrue(inuzumaRamen.getTopping() instanceof BoiledEgg);
    }

    @Test
    public void testInuzumaRamenGetFlavor() {
        assertTrue(inuzumaRamen.getFlavor() instanceof Spicy);
    }
}
