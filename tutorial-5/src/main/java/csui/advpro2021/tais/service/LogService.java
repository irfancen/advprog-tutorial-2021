package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.LogSummary;
import csui.advpro2021.tais.model.Mahasiswa;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

@Service
public interface LogService {
    Log createLog(Mahasiswa mahasiswa, Log log);

    Iterable<Log> getListLog();

    Log getLogByIdLog(Integer idLog);

    List<Log> getAllLogByMahasiswa(Mahasiswa mahasiswa);

    Collection<LogSummary> getLogSummary(Mahasiswa mahasiswa);

    Log updateLog(Integer idLog, Mahasiswa mahasiswa, Log log);

    void deleteLogByIdLog(Integer idLog);
}
