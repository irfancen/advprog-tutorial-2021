package id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.flavor;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class SaltyTest {
    @Test
    public void testSaltyDescription() {
        Salty salty = new Salty();
        assertEquals("Adding a pinch of salt...", salty.getDescription());
    }
}
