package id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.noodle;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class UdonTest {
    @Test
    public void testUdonDescription() {
        Udon udon = new Udon();
        assertEquals("Adding Mondo Udon Noodles...", udon.getDescription());
    }
}
