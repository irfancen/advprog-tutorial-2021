package id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.topping;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class CheeseTest {
    @Test
    public void testCheeseDescription() {
        Cheese cheese = new Cheese();
        assertEquals("Adding Shredded Cheese Topping...", cheese.getDescription());
    }
}
