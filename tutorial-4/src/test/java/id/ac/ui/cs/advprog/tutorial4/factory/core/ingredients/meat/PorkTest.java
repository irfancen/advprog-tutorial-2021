package id.ac.ui.cs.advprog.tutorial4.factory.core.ingredients.meat;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class PorkTest {
    @Test
    public void testPorkDescription() {
        Pork pork = new Pork();
        assertEquals("Adding Tian Xu Pork Meat...", pork.getDescription());
    }
}
