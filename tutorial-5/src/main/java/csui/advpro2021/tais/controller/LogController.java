package csui.advpro2021.tais.controller;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.LogSummary;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.service.LogService;
import csui.advpro2021.tais.service.MahasiswaService;
import csui.advpro2021.tais.service.MataKuliahService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/asisten")
public class LogController {
    @Autowired
    private LogService logService;

    @Autowired
    private MahasiswaService mahasiswaService;

    @Autowired
    private MataKuliahService mataKuliahService;


    @GetMapping(path="/logs", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<Log>> getListLog() {
        return ResponseEntity.ok(logService.getListLog());
    }

    @GetMapping(path = "/log/{idLog}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity getLog(@PathVariable(value = "idLog") Integer idLog) {
        Log log = logService.getLogByIdLog(idLog);
        if (log == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(log);
    }

    @PutMapping(path = "/log/{idLog}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity updateLog(@PathVariable(value = "idLog") Integer idLog, @RequestBody Log log) {
        Log logS = logService.getLogByIdLog(idLog);
        if (logS == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        Mahasiswa mahasiswa = logService.getLogByIdLog(idLog).getMahasiswa();
        return ResponseEntity.ok(logService.updateLog(idLog, mahasiswa, log));
    }

    @DeleteMapping(path = "/log/{idLog}", produces = {"application/json"})
    public ResponseEntity deleteLog(@PathVariable(value = "idLog") Integer idLog) {
        Log log = logService.getLogByIdLog(idLog);
        if (log == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        logService.deleteLogByIdLog(idLog);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @PostMapping(path = "/daftar/{npm}/{kodeMatkul}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity daftarAsisten(@PathVariable(value = "npm") String npm, @PathVariable(value = "kodeMatkul") String kodeMatkul) {
        Mahasiswa mahasiswa = mahasiswaService.getMahasiswaByNPM(npm);
        if (mahasiswa == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        MataKuliah mataKuliah = mataKuliahService.getMataKuliah(kodeMatkul);
        if (mataKuliah == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(mahasiswaService.addAsisten(mahasiswa, mataKuliah));
    }

    @GetMapping(path = "/summary/{npm}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<LogSummary>> getSummary(@PathVariable(value = "npm") String npm) {
        Mahasiswa mahasiswa = mahasiswaService.getMahasiswaByNPM(npm);
        if (mahasiswa == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(logService.getLogSummary(mahasiswa));
    }

    @PostMapping(path = "/create/{npm}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity tambahLog(@PathVariable(value = "npm") String npm, @RequestBody Log log) {
        Mahasiswa mahasiswa = mahasiswaService.getMahasiswaByNPM(npm);
        if (mahasiswa == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(logService.createLog(mahasiswa, log));
    }

    @GetMapping(path = "/logs/{npm}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity getLogMahasiswa(@PathVariable(value = "npm") String npm) {
        Mahasiswa mahasiswa = mahasiswaService.getMahasiswaByNPM(npm);
        if (mahasiswa == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(logService.getAllLogByMahasiswa(mahasiswa));
    }
}
